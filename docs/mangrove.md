---
title: Mangrove
sidebarDepth: 2
---

<Tree type="Mangrove" />

As consumer choice proliferates, people will rely even more on online reviews to inform their decisions. This makes reviews an increasingly powerful tool and source of insight for individuals and businesses alike. The problem is that hundreds of millions of people are feeding their insights currently into proprietary data silos of a few dominant platforms. We want to change this!    
    
**Mangrove is a non-profit initiative to create a public space on the Internet where people can freely share insights with each other and make better decisions based on open data.**

The host organisation is the [Open Reviews Association](https://open-reviews.net) and you can **try things out** or learn about the technology via the [Mangrove Original UI](https://mangrove.reviews).

Our aim is to **create an open data ecosystem for reviews of places, companies, websites, books, and more**. We built infrastructure that enables any application or website to integrate a reviews layer, and to benefit from the shared data pool that is created by a combined user base of participating applications. We also provide a web app that can be used by people directly, and that serves as an uncensorable source of truth.

## To move away from proprietary data silos, we need to change the fundamental architecture 
To instantiate openness and privacy, we are proposing an alternative architecture that is characterized by a **separation of data and product**, and that is designed to respect the right to privacy:

* Review **data**, representing the insights, knowledge and wisdom of the public, should be open and freely available to all. As such, it provides a valuable foundation for many use-cases, including research, social or commercial purposes.

* **Products** (i.e., applications and services) that are built upon crowd-sourced data should base their business models on innovation and technological merit, and not on locking in users and closing out competition. An architecture that separates data from product helps remove barriers to entry and avoids information silos.

* The **privacy** of users has to be protected by design, not by promises. Mangrove uses public-key cryptography instead of traditional logins to establish an identity for a user. This allows us to create a system where the user doesn’t have to register and entrust yet another service with their name, email address, birth date, etc.

There are successful examples of non-profit data layers that power whole ecosystems of products built upon them. One such example is [OpenStreetMap](https://www.openstreetmap.org/about), an initiative to create and provide free geographic data, such as street maps, to anyone. With a community of [over 6 million](https://www.openstreetmap.org/stats/data_stats.html) registered users who have jointly uploaded 7.5 billion GPS points, OpenStreetMap powers map data on thousands of web sites, mobile apps, and hardware devices for commercial, humanitarian or social purposes. 

We hope that the Mangrove open data set for reviews can become a valuable additional data layer to make community-driven and open source projects more competitive against proprietary services and data monopolists, and allow people to innovate with less barriers to entry.

## Features by use case

* **For people who read and write reviews**
    * Write and share reviews for places, local businesses, websites, companies, and books
    * Add photos to your reviews to make them more useful.
    * Help maintain a high-quality dataset by upvoting helpful reviews and flagging unhelpful ones.
    * Build a reputation and profile for yourself.
    * Do all this without being tracked and exploited: you control what information you share about yourself.
    * Rest assured that the community maintains the dataset, not a profit-oriented business.
    
* **For entrepreneurs and developers**
    * Share and re-use the review dataset under a free and open license [(CC BY 4.0)](https://creativecommons.org/licenses/by/4.0/).
    * Integrate reviews in your application or website to improve your users' experience:
        * Maps and navigation apps
        * Travel and tourist guide websites
        * Consumer protection initiatives
        * Advocates of corporate transparency
        * Recommendation services
        * ...and many more
    * Build a business, initiative or a research project on this data
        * Provide brand and reputation management services to local businesses
        * Develop alternative aggregation algorithms and APIs
        * Innovate and create new recommendation products without barriers to entry
        * Research consumer sentiment and brand developments
        * Surface consumer protection issues and build momentum to improve the status quo

* **For local businesses, companies and websites**
    * Show what your customers love about you by embedding reviews into your website
    * Allow your customers to write reviews based on verified purchases
    * Answer reviews and engage with your customers
    * Manage your reputation by analysing review data
    * Compare yourself to your competitors and recognise trends

* **For all**
    * Enjoy a high level of reliability without the suspicion of censorship as we use 100% open probabilistic models and a variety of fully transparent methods to keep the data as clean as possible.

## FAQ

### Why should people care about reviews?

We all benefit from leveraging the experiences of others for our own decision-making. We feel an intrinsic need to share with others information about things we used and experienced, and to make use of such information ourselves before we invest resources.  
   
The world is growing more complex, and so are the choices that we have to make each day. The number of services, products, media outlets, books, travel destinations, websites, apps, is growing, and it takes more effort to navigate the global range of offerings. We increasingly look up information online when we want to buy something or go somewhere, and we actively search for other people's opinions in order to form our own (research studies performed by IMC or BrightLocal have quantified this trend).   
    
In addition, the opinions of customers in the form of online reviews have become crucial for businesses. Online word-of-mouth impacts businesses' reputation, their ranking in search results, and ultimately their profitability, as consumers are willing to pay more for products for which reviews are available.   
    
Companies such as Google, Yelp, Facebook, TripAdvisor, or TrustPilot have recognized this trend already years ago, and offered business listing platforms on which users could leave reviews as well as read other people's reviews for free. By now, these services boast hundreds of millions of consumer reviews in their applications. The crowd-sourced data has become a well-guarded asset for these companies, based on which they generate revenues, predominantly from advertising and sharing the personal data of their users.

### Why is the current state that we are in bad?

The current model in which a small number of dominating platforms keep reviews proprietary and exploit reviewers' and users' personal data for profit, is not sustainable. We see a number of issues:
* **Data partitioning**: reviews for the same item are split across several platforms, without aggregation
    * people searching for reviews have to look in several places 
    * people willing to write a review have to decide each time on which platform to leave their opinion
    * each platform attracts a limited demographic, leading to potential biases    
        
* **Reviewer exploitation**: users provide reviews freely, yet the data is owned and capitalized on by the platform, while users' privacy is invaded for targeted advertisement - by consumer product marketers, as well as by political campaigns

* **Intransparency**: the data as well as the algorithms that compute the rating and determine which reviews are shown and which are deleted, are not open to the public
    * lack of control for the public over what happens with the data
    * possibility of censorship
    * possibility that reviews are lost forever if the platform gets closed down    
         
* **Barriers for innovation**: closed-source data disallows creators to create new and useful tools; high barriers for leveraging this rich information

### Why does the world need an open data set for reviews?

We believe the ability of people to articulate and share freely their insights, assessments and opinions on anything that is public, is core to a free civil society. Large corporations are gaining power and are increasingly hard to control. Marketers, and even political campaigns, are using insights from the vast amount of personal data to manipulate the choices people make. Individuals and communities need to have a way to share information and coordinate freely in order to be able to create a counter-balance. With this tool, open to all, people can help each other to navigate and shape the increasingly complex space of choices we all have today.

### How can this setup ensure reliable reviews?

Being an open data set that is maintained in a decentralized manner should not impact the reliability of the data negatively. There should not be a possibility for deletion and censorship of genuine reviews. Fraudulent reviews and irrelevant posts should not be taken into account when composing the rating. Furthermore, the data set should not be a place for hate speech. 

Mangrove's approach to avoid content that is damaging the reliability of reviews:
* Businesses can invite their customers to leave reviews in the Mangrove database, allowing to mark those reviews as 'verified purchases'.
* Businesses can interact with their customers and reply to reviews.
* Users can mark reviews as helpful, so that high quality reviews have a higher weight in the aggregated rating.
* Users can get a 'reliability score', reflecting their track record in providing helpful reviews, thereby increasing their influence on the aggregated rating.
* The Mangrove filtering algorithm uses probabilistic models to identify fraudulent reviews and devalue them, as well as flag them as probably fraudulent to the viewer
