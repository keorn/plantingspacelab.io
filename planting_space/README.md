Static site built with [Hugo](https://gohugo.io/).

To view run `hugo server`, to build in `public` directory run `hugo`.
