---
title: Julia developer
img: /img/julia_ico.svg
alt: Julia developer
type: job
experience: Backend software engineering;Scientific computing;Julia metaprogramming;Bayesian statistics;Optimization algorithms
expBrackets: big
responsibilities: Develop Julia based on clear requirements;Estimate work required for tasks
resBrackets: ps-small
sort: 1
---

We are looking for top software developers to contribute to development
of core components. Work time will be flexible and tasks will focus on general
development, stats or optimization. See [here](https://gitlab.com/plantingspace/tasks/-/issues/1) for an example task that you could work on.
